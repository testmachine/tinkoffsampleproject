//
//  ASDKSampleAppUITests.swift
//  ASDKSampleAppUITests
//
//  Created by MacBookPro on 04/12/2017.
//  Copyright © 2017 TCS Bank. All rights reserved.
//

import XCTest

class ASDKSampleAppUITests: XCTestCase {
    
    var app: XCUIApplication!
    
    override func setUp() {
        super.setUp()
        continueAfterFailure = false
        app = XCUIApplication()
        app.launch()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testExample() {
        let shopItemName = "Шантарам - 2. Тень горы"
        
        let catalogScreen = CatalogScreen(app: app)
        let itemScreen = catalogScreen.selectItemBy(text: shopItemName)
        let currentPrice = itemScreen.priceLabel.label
        var paymentScreen =  itemScreen.buy()
        
        paymentScreen = paymentScreen.changeCard().selectNewCard()
        paymentScreen.fillCardData(card: Cards.visa)
        let successfulPaymentScreen = paymentScreen.pay()
        
        XCTAssertTrue(successfulPaymentScreen.paymentInfoLabel(price: currentPrice).exists)
    }
    
}
