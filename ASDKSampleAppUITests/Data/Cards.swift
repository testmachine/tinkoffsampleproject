//
//  Cards.swift
//  ASDKSampleAppUITests
//
//  Created by MacBookPro on 06/12/2017.
//  Copyright © 2017 TCS Bank. All rights reserved.
//

import Foundation

struct Card {
    let number: String
    let expiration: String
    let cvv: String
}

class Cards {
    static let masterCard = Card(number: "5000000000000777", expiration: "1220", cvv: "749")
    static let visa = Card(number: "4300000000000777", expiration: "1220", cvv: "749")
    static let mir = Card(number: "2201382000000021", expiration: "1220", cvv: "749")
}
