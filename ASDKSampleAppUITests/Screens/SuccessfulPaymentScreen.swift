//
//  SuccessfulPaymentScreen.swift
//  ASDKSampleAppUITests
//
//  Created by MacBookPro on 06/12/2017.
//  Copyright © 2017 TCS Bank. All rights reserved.
//

import XCTest

class SuccessfulPaymentScreen: ScreenBase {    
    func paymentInfoLabel(price: String) -> XCUIElement {
        return self.staticTexts.matching(NSPredicate(format: "(label CONTAINS '\(price)') AND (label CONTAINS 'success')")).firstMatch
    }
}
