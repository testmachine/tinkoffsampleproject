//
//  CatalogScreen.swift
//  ASDKSampleAppUITests
//
//  Created by MacBookPro on 06/12/2017.
//  Copyright © 2017 TCS Bank. All rights reserved.
//

import XCTest

class CatalogScreen: ScreenBase {
    func detailsButtonBy(text: String) -> XCUIElement  {
        return self.tables.cells.containing(.staticText, identifier: text).buttons["Details"]
    }
    
    func selectItemBy(text: String) -> ItemScreen {
        detailsButtonBy(text: text).tap()
        return ItemScreen(app: self.app)
    }
}
