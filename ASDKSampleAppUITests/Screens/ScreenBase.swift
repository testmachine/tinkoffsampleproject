//
//  ScreenBase.swift
//  ASDKSampleAppUITests
//
//  Created by MacBookPro on 06/12/2017.
//  Copyright © 2017 TCS Bank. All rights reserved.
//

import XCTest

class ScreenBase {
    let app: XCUIApplication
    
    let tables: XCUIElementQuery
    
    let buttons: XCUIElementQuery
    
    let staticTexts: XCUIElementQuery
    
    let textFields: XCUIElementQuery
    
    init(app: XCUIApplication) {
        self.app = app
        self.tables = app.tables
        self.buttons = app.buttons
        self.staticTexts = app.staticTexts
        self.textFields = app.textFields
    }
}
