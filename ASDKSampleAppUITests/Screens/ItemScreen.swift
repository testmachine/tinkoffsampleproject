//
//  ItemScreen.swift
//  ASDKSampleAppUITests
//
//  Created by MacBookPro on 06/12/2017.
//  Copyright © 2017 TCS Bank. All rights reserved.
//

import XCTest

class ItemScreen: ScreenBase {
    var buyButton: XCUIElement {
        return self.buttons["BUY"]
    }
    
    var priceLabel: XCUIElement {
        return self.staticTexts.matching(NSPredicate(format: "label CONTAINS '₽'")).firstMatch
    }
    
    func buy() -> PaymentScreen {
        buyButton.tap()
        return PaymentScreen(app: self.app)
    }
}
