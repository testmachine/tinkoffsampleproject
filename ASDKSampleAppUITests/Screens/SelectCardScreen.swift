//
//  SelectCardScreen.swift
//  ASDKSampleAppUITests
//
//  Created by MacBookPro on 06/12/2017.
//  Copyright © 2017 TCS Bank. All rights reserved.
//

import XCTest

class SelectCardScreen: ScreenBase {
    var addNewCardButton: XCUIElement {
        return self.staticTexts["New card number"]
    }
    
    func selectNewCard() -> PaymentScreen {
        addNewCardButton.tap()
        return PaymentScreen(app: self.app)
    }
}
