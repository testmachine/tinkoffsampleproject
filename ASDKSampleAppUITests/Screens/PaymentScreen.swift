//
//  PaymentScreen.swift
//  ASDKSampleAppUITests
//
//  Created by MacBookPro on 06/12/2017.
//  Copyright © 2017 TCS Bank. All rights reserved.
//

import XCTest

class PaymentScreen: ScreenBase {
    var payButton: XCUIElement {
        return self.staticTexts["Pay"]
    }
    
    var changeCardButton: XCUIElement {
        return self.tables/*@START_MENU_TOKEN@*/.buttons["сhange"]/*[[".cells.buttons[\"сhange\"]",".buttons[\"сhange\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
    }
    
    var cardNumberField: XCUIElement {
        return self.tables.cells.containing(.textField, identifier: "MM/YY").children(matching: .textField).element(boundBy: 0)
    }
    
    var expirationField: XCUIElement {
        return self.textFields["MM/YY"]
    }
    
    var cvvField: XCUIElement {
        return self.tables.secureTextFields["CVV"]
    }
    
    func changeCard() -> SelectCardScreen {
        changeCardButton.tap()
        return SelectCardScreen(app: self.app)
    }
    
    func typeCardNumber(cardNumber: String) {
        cardNumberField.tap()
        cardNumberField.typeText(cardNumber)
    }
    
    func typeCVV(cvv: String) {
        cvvField.tap()
        cvvField.typeText(cvv)
    }
    
    func typeExpiration(expiration: String) {
        expirationField.tap()
        expirationField.typeText(expiration)
    }
    
    func fillCardData(card: Card) {
        typeCardNumber(cardNumber: card.number)
        typeExpiration(expiration: card.expiration)
        typeCVV(cvv: card.cvv)
    }
    
    func pay() -> SuccessfulPaymentScreen {
        payButton.tap()
        return SuccessfulPaymentScreen(app: self.app)
    }
}
