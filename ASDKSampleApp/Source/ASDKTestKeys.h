//
//  ASDKTestKeys.h
//  ASDKSampleApp
//
//  Created by Макс Жданов on 08.02.16.
//  Copyright © 2016 TCS Bank. All rights reserved.
//

#ifndef ASDKTestKeys_h
#define ASDKTestKeys_h

/*
replace this constants:
 kASDKTestTerminalKey
 kASDKTestPassword
 kASDKTestPublicKey
*/
#warning FIX ME!!!

//#define kASDKTestTerminalKey1 @"testRegress"
#define kASDKTestTerminalKey1 @"TestSDK"
//#define kASDKTestTerminalKey1 @"1486655771621"
#define kASDKTestTerminalKey2 @"sdk3DS"
#define kASDKTestTerminalKey3 @"sdkNon3DS"

#define kASDKTestPassword @"12345678"
//#define kASDKTestPassword @"istkraxynrgfwgov"

//#define kASDKTestPublicKey @"MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAv5yse9ka3ZQE0feuGtemYv3IqOlLck8zHUM7lTr0za6lXTszRSXfUO7jMb+L5C7e2QNFs+7sIX2OQJ6a+HG8kr+jwJ4tS3cVsWtd9NXpsU40PE4MeNr5RqiNXjcDxA+L4OsEm/BlyFOEOh2epGyYUd5/iO3OiQFRNicomT2saQYAeqIwuELPs1XpLk9HLx5qPbm8fRrQhjeUD5TLO8b+4yCnObe8vy/BMUwBfq+ieWADIjwWCMp2KTpMGLz48qnaD9kdrYJ0iyHqzb2mkDhdIzkim24A3lWoYitJCBrrB2xM05sm9+OdCI1f7nPNJbl5URHobSwR94IRGT7CJcUjvwIDAQAB"
#define kASDKTestPublicKey @"MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA5Udq+T7YwQ8tINcCaYQUvaKs6VGIrWp+S3CHq84saegSGARQAPdu/KXrlcGGg5O3eGp6xJWmcZIeob+T7qHWtiQyWO9MgP32QKt8bqAp2XwbaGVMH8XzeOMuM1b/lxjkZXjMcA4VMccanKWU6LIXWGiS3mdu6GB4ucTrUDKF94CPrNGh+Ey6gKBMbkNpmR6g2LtaF02emWKp7vHWzA37W7UgMJr3ppdSbFPBmioF4xRlqeeVmLBm9hptsD5aFXitKSdmQ86LSgN6mnwinQoGq5Huqe93pmBRELvFy1bO3gPCiRGEwf0Kxln1qibtZsA5DC3Y/5P6umxG10g6QUCQVwIDAQAB"

#endif /* ASDKTestKeys_h */
